import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import static org.assertj.core.api.Assertions.*

if (finishRow == 0) {
	finishRow = findTestData('credential').getRowNumbers()
}


for (startRow; startRow <= finishRow; startRow++) {
	String tc = findTestData('credential').getValue(1, startRow)
	String username = findTestData('credential').getValue(2, startRow)
	String password = findTestData('credential').getValue(3, startRow) 
	res = WS.sendRequestAndVerify(findTestObject('Auth/Auth - Create Token', [('BASE_URL') : GlobalVariable.baseUrl, ('ENDPOINT') : 'auth', ('url') : '${BASE_URL}/${ENDPOINT}'
	            , ('username') : username, ('password') : password]))
	
	if (tc == '001' || tc == '002' || tc == '003') {
		assertThat(res.getResponseText()).contains('reason')
		WS.verifyElementPropertyValue(res, 'reason', 'Bad credentials')
		println("AUTH-${tc} is Passed")
	} else if (tc == '004') {
		assertThat(res.getResponseText()).contains('token')
		println("AUTH-${tc} is Passed")
		GlobalVariable.token = WS.getElementPropertyValue(res, 'token')
		println(GlobalVariable.token)
	} else {
		println("AUTH-${tc} is Passed")
	}
		
}


