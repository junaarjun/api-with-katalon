import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import static org.assertj.core.api.Assertions.*

//Mobile.callTestCase(findTestCase('Auth/AUTH-001'), ["startRow":4, "finishRow":4], FailureHandling.STOP_ON_FAILURE)

res = WS.sendRequestAndVerify(findTestObject('Booking/GetBoookingIds', [('base_url') : GlobalVariable.baseUrl, ('endpoint') : 'booking'
            , ('url') : '${base_url}/${endpoint}', ('token') : GlobalVariable.token]))

WS.verifyResponseStatusCode(res, 200)

assertThat(res.getResponseText()).contains('bookingid')

println('BOOK-001 is Passed')

int randNum = Math.abs(new Random().nextInt() % 1500 + 1)

GlobalVariable.bookingID = WS.getElementPropertyValue(res, "bookingid[${randNum}]")
println('BookingID: '+ GlobalVariable.bookingID)



